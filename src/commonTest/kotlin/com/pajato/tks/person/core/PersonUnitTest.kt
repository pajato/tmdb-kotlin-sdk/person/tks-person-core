package com.pajato.tks.person.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class PersonUnitTest : ReportingTestProfiler() {
    @Test fun `When serializing a default person, verify behavior`() {
        val person = Person()
        assertEquals("{}", jsonFormat.encodeToString(person))
    }
}
