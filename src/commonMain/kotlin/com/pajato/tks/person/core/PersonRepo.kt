package com.pajato.tks.person.core

import com.pajato.tks.common.core.PersonKey

public interface PersonRepo {
    public suspend fun getPerson(key: PersonKey): Person
}
