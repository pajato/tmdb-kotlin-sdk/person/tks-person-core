package com.pajato.tks.person.core

import com.pajato.tks.common.core.TmdbId
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Person(
    val adult: Boolean = false,
    @SerialName("also_known_as")
    val alsoKnownAs: List<String> = listOf(),
    val biography: String = "",
    val birthday: String = "",
    val deathday: String = "",
    val gender: Int = 0,
    val homepage: String = "",
    val id: TmdbId = 0,
    @SerialName("imdb_id")
    val imdbId: String = "",
    @SerialName("known_for_department")
    val knownForDepartment: String = "",
    val name: String = "",
    @SerialName("place_of_birth")
    val placeOfBirth: String = "",
    val popularity: Double = 0.0,
    @SerialName("profile_path")
    val profilePath: String = "",
)
